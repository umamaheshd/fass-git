from xpms_helper.executions.execution_variables import ExecutionVariables


def get_execution_variables(**kwargs):
    context = {}
    exec_ins = ExecutionVariables.get_instance(context)
    output = exec_ins.get_variable('df')
    return output, kwargs

get_execution_variables()