def test_func_multiply(num1, num2, config=None):
    if num1 >= 0 and num2 >= 0:
        return num1 * num2
    else:
        value = num1 + num2
        return value
